package config

import "os"

type DaemonEndPoint struct {
	Url string
	Port string
}

func New() *DaemonEndPoint {
	endPoint := os.Getenv("DAEMON_URI")
	port := os.Getenv("DAEMON_PORT")

	if len(endPoint) == 0  {
		endPoint = "localhost"
	}

	if len(port) == 0 {
		port = "4280"
	}

	return &DaemonEndPoint{
		Url: endPoint,
		Port: port,
	}
}

func (d *DaemonEndPoint) EndPointUrl() string {
	return d.Url + ":" + d.Port
}
