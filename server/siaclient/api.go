package siaclient

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/scpcorp/ScPrime/node/api/client"
	//"gitlab.com/scpcorp/explorers/server/config"
)

// We use Client package to communicate with SPD
var (
	//daemonUrl     = config.New().EndPointUrl()
	daemonOptions, _ = client.DefaultOptions()
	daemonUrl        = daemonOptions.Address
	httpClient       = client.New(daemonOptions)
	
)

// HttpGET is a utility function for making http get requests to sia with a
// whitelisted user-agent. A non-2xx response does not return an error.
func httpGET(url string) (resp *http.Response, err error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	userAgent := os.Getenv("SCP_AGENT")

	if userAgent == "" {
		userAgent = "ScPrime-Agent"
	}
	
	req.Header.Set("User-Agent", userAgent)
	return http.DefaultClient.Do(req)
}

func drainAndClose(rc io.ReadCloser) {
	io.Copy(ioutil.Discard, rc)
	rc.Close()
}

// get requests the specified resource. The response, if provided, will be
// decoded into obj. The resource is the full API url.
func get(resource string, obj interface{}) error {
	// Request resource
	data, err := getRawResponse(resource)
	if err != nil {
		return err
	}
	if obj == nil {
		// No need to decode response
		return nil
	}

	if err := json.Unmarshal(data, obj); err != nil {
		panic(err)
	}

	//TODO: figure out what this does
	/*if err != nil {
		return errors.AddContext(err, "could not read response")
	}*/

	return nil
}

// get the raw http response and read body
func getRawResponse(resource string) ([]byte, error) {
	resp, err := httpGET(resource)
	fmt.Println("HTTP_GET:", resp, err)
	if err != nil {
		return nil, errors.AddContext(err, "request failed")
	}
	defer drainAndClose(resp.Body)

	if resp.StatusCode == http.StatusNotFound {
		return nil, errors.New("API call not recognized: " + resource)
	}

	if resp.StatusCode == http.StatusNoContent {
		// no reason to read the response
		return []byte{}, nil
	}
	return ioutil.ReadAll(resp.Body)
}
