package siaclient

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)


// We are calling Sia's API and depend on Sia to handle object-type strictness
// In order to avoid duplicating and more work of handling object-strictness, we declare generic interfaces
// to unmarshall the json data returned by Sia
var (
	explorerGet map[string] interface{}
	explorerBlockGet map[string] interface{}
	explorerHashGet map[string] interface{}
)
func ExplorerStatusHandler(w http.ResponseWriter, r *http.Request) {
	apiErr := get("http://" + daemonUrl + "/explorer", &explorerGet)

	if apiErr != nil {
		fmt.Println("GetAllHost ERR: \n", apiErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// return json
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(explorerGet)

	if _, isJsonErr := err.(*json.SyntaxError); isJsonErr {
		panic("failed to return json")
	}
}

func ExplorerBlocksHandler(w http.ResponseWriter, r *http.Request) {
	// get params
	height := mux.Vars(r)["height"]

	if len(height) == 0 {
		fmt.Println("ExplorerBlocksHandler Invalid Params ERROR")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	apiErr := get("http://" + daemonUrl + "/explorer/blocks/" + height, &explorerBlockGet)

	if apiErr != nil {
		fmt.Println("GetExplorerHeight ERR: \n", apiErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// return json
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(explorerBlockGet)

	if _, isJsonErr := err.(*json.SyntaxError); isJsonErr {
		panic("failed to return json")
	}
}
/*
* @params - hash
* calls daemon API - explorerHashHandler. Daemon API checks hash for a "HashType" of
* blockid | transactionid | siacoinoutputid | filecontractid
*
* Try hash id as block id, transaction id, siacoin output id, file contract id, siafund id
* return json track types by field "hashtype"
*/
func ExplorerHashHandler(w http.ResponseWriter, r *http.Request) {
	explorerHashGet = nil
	hash := mux.Vars(r)["hash"]

	if len(hash) == 0 {
		fmt.Println("ExplorerBlocksHashHandler Invalid Params ERROR")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	apiErr := get("http://" + daemonUrl + "/explorer/hashes/" + hash, &explorerHashGet)

	if apiErr != nil {
		fmt.Println("GetExplorerHash ERR: \n", apiErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// return json
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(explorerHashGet)

	if _, isJsonErr := err.(*json.SyntaxError); isJsonErr {
		panic("failed to return json")
	}
}