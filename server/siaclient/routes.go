package siaclient

import (
	"gitlab.com/scpcorp/explorers/server/types"
)

var Routes = types.Routes {
	{
		"GetAllHost",
		"GET",
		"/hostdb/all",
		AllHostDBHandler,
	},
	{
		"GetActiveHost",
		"GET",
		"/host/active",
		ActiveHostHandler,
	},
	{
		"GetExplorerStatus",
		"GET",
		"/explorer",
		ExplorerStatusHandler,
	},
	{
		"GetExplorerBlocks",
		"GET",
		"/explorer/blocks/{height}",
		ExplorerBlocksHandler,
	},
	{
		"GetExplorerHash",
		"GET",
		"/explorer/hashes/{hash}",
		ExplorerHashHandler,
	},
	{
		"GetConsensus",
		"GET",
		"/consensus",
		ConsensusStatusHandler,
	},
	{
		"GetBlock",
		"GET",
		"/consensus/blocks",
		ConsensusBlocksHandler,
	},
}
