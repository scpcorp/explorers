package siaclient

import (
	"encoding/json"
	"fmt"
	"net/http"
)


// This file is a wrapper that handles our http request and
// uses Sia's Client package to communicate with SPD to retrieve data.
// For convention and easy to understand, any Sia route path will be kept the same
// as Sia's apis.


func AllHostDBHandler(w http.ResponseWriter, r *http.Request) {
	resp, clientErr := httpClient.HostDbAllGet()

	if clientErr != nil {
		fmt.Println("GetAllHost ERR: \n", clientErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(resp.Hosts) == 0 {
		http.Error(w, "No host Found", http.StatusNotFound)
		return
	}

	// return json
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(resp)

	if _, isJsonErr := err.(*json.SyntaxError); isJsonErr {
		panic("failed to return json")
	}


}


func ActiveHostHandler(w http.ResponseWriter, r *http.Request) {
	resp, clientErr := httpClient.HostDbActiveGet()

	if clientErr != nil {
		fmt.Println("GetActiveHost ERR: \n", clientErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// return json
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(resp)
	if _, isJsonErr := err.(*json.SyntaxError); isJsonErr {
		panic("failed to return json")
	}
}