package elastic

import "gitlab.com/scpcorp/explorers/server/types"

var Routes = types.Routes {
	{
		"SearchDocID",
		"GET",
		"/search/{id}",
		SearchDocIDHandler,
	},
	{
		"GetLatestProvidersScans",
		"GET",
		"/current-scans",
		LatestProvidersScansHandler,
	},
}
