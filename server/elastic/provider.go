package elastic

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

type Hits struct {
	Hits []interface{} `json:"scans"`
}

func (h *Hits) AddItem(p interface{}) []interface{} {
	h.Hits = append(h.Hits, p)
	return h.Hits
}

func LatestProvidersScansHandler(w http.ResponseWriter, r *http.Request) {
	body := strings.NewReader(
		`{ "size": 500,
			  "script_fields": {
				"hostscans": {
					"script": {
						"source": "def scans = params['_source']['hostscans'];return scans[scans.length -1];",
						"lang": "painless"
						}
					}
				} 
			}`)

	res, esErr := Es7.Search(
		Es7.Search.WithContext(ctx),
		Es7.Search.WithIndex("test1_hostdb_scan"),
		Es7.Search.WithBody(body),
	)

	if esErr != nil {
		fmt.Println("GetProviderLatestScans ERR:", esErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if res.StatusCode != 200 {
		fmt.Println("ElasticSearch Res Code:", res.StatusCode)
		w.WriteHeader(http.StatusBadRequest)
		log.Println("ERR", res.String())
		return
	}

	defer res.Body.Close()

	data, readErr := getElasticResponse(res)

	if readErr != nil{
		panic(readErr)
	}

	if uErr := json.Unmarshal(data, &esResponse); uErr != nil {
		panic(uErr)
	}

	resArr := esResponse["hits"].(map[string]interface{})
	hits := resArr["hits"].([]interface{})

	var results Hits
	for _, v := range hits {
		host := v.(map[string]interface{})["fields"].(map[string]interface{})["hostscans"].([]interface{})
		results.AddItem(host[0])
	}


	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(results)

	if _, isJsonErr := err.(*json.SyntaxError); isJsonErr {
		panic("failed to return json")
	}

}