package elastic

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

var (
	Es7 *elasticsearch.Client
	ctx = context.Background()
	esResponse map[string] interface{}
)

func SearchDocIDHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	body := strings.NewReader(fmt.Sprintf(`{"query": { "term": {"_id": "%v"} } }`, id))

	res, esErr := Es7.Search(
		Es7.Search.WithContext(ctx),
		Es7.Search.WithIndex("_all"),
		Es7.Search.WithBody(body),
	)

	if esErr != nil {
		fmt.Println("SearchDocID ERR:", esErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if res.StatusCode != 200 {
		fmt.Println("ElasticSearch Res Code:", res.StatusCode)
		w.WriteHeader(http.StatusBadRequest)
		log.Println("ERR", res.String())
		return
	}

	// Close the result body when the function call is complete
	defer res.Body.Close()

	data, readErr := getElasticResponse(res)

	if readErr != nil{
		panic(readErr)
	}

	if uErr := json.Unmarshal(data, &esResponse); uErr != nil {
		panic(uErr)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(esResponse)
	
	if _, isJsonErr := err.(*json.SyntaxError); isJsonErr {
		panic("failed to return json")
	}

}

func getElasticResponse(res *esapi.Response) ([]byte, error) {
	return ioutil.ReadAll(res.Body)
}