package router

import (
	"gitlab.com/scpcorp/explorers/server/elastic"
	"gitlab.com/scpcorp/explorers/server/luxorapi"
	"gitlab.com/scpcorp/explorers/server/siaclient"
	"gitlab.com/scpcorp/explorers/server/types"
)

var ApplicationRoutes []types.Route

func compileRoutes() {
	routes := []types.Routes{
		luxorapi.Routes,
		siaclient.Routes,
		elastic.Routes,
	}

	for _, r := range routes {
		ApplicationRoutes = append(ApplicationRoutes, r...)
	}
}


