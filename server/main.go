package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"gitlab.com/scpcorp/explorers/server/elastic"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/rs/cors"
	"gitlab.com/scpcorp/explorers/server/router"
)

// setupGlobalMiddleware will setup CORS
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	log.Println(handler)
	handleCORS := cors.Default().Handler
	return handleCORS(handler)
}

func main() {
	serverEnv := os.Getenv("SERVER_ENV")
	routerMux := router.NewRouter()

	es, err := elasticsearch.NewClient(elasticsearch.Config{Addresses: []string{"http://scprime-dev-01.scpri.me:9200"}})

	if err != nil {
		log.Fatalf("Elasticsearch connection error:", err)
	}

	elastic.Es7 = es

	if serverEnv == "PRODUCTION" {
		var wait time.Duration
		flag.DurationVar(&wait, "graceful-timeout", time.Second * 15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
		flag.Parse()

		fmt.Println("Listening on port 3000")
		router.ServeSPA(routerMux)
	
		// Good practice to set timeouts to avoid Slowloris attacks
		srv := &http.Server {
			Addr: "0.0.0.0:8080",
			WriteTimeout: time.Second * 15,
			ReadTimeout: time.Second * 15,
			IdleTimeout: time.Second * 60,
			Handler: setupGlobalMiddleware(routerMux),
		}

		// Run our server in a goroutine so that it doesn't block.
		go func() {
			if err := srv.ListenAndServe(); err != nil {
				log.Println(err)
			}
		}()
	
		c := make(chan os.Signal, 1)
		// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
		// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
		signal.Notify(c, os.Interrupt)
	
		// Block until we receive our signal.
		<-c
	
		// Create a deadline to wait for.
		ctx, cancel := context.WithTimeout(context.Background(), wait)
		defer cancel()
		// Doesn't block if no connections, but will otherwise wait
		// until the timeout deadline.
		srv.Shutdown(ctx)
		// Optionally, you could run srv.Shutdown in a goroutine and block on
		// <-ctx.Done() if your application should wait for other services
		// to finalize based on context cancellation.
		log.Println("shutting down")
		os.Exit(0)			
	} else {
		fmt.Println("Running in development mode, listening on port 5001, proxying port 5000")
		log.Fatal(http.ListenAndServe(":5001", setupGlobalMiddleware(routerMux)))
	}
}
