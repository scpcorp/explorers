module gitlab.com/scpcorp/explorers/server

go 1.14

require (
	github.com/0xAX/notificator v0.0.0-20191016112426-3962a5ea8da1 // indirect
	github.com/codegangsta/envy v0.0.0-20141216192214-4b78388c8ce4 // indirect
	github.com/codegangsta/gin v0.0.0-20171026143024-cafe2ce98974 // indirect
	github.com/elastic/go-elasticsearch/v7 v7.5.1-0.20200925065759-6190bd8a763e
	github.com/gorilla/mux v1.7.4
	github.com/mattn/go-shellwords v1.0.10 // indirect
	github.com/rs/cors v1.7.0
	gitlab.com/NebulousLabs/errors v0.0.0-20171229012116-7ead97ef90b8
	gitlab.com/scpcorp/ScPrime v1.4.2-0.20200814114214-196c147f9828
	gopkg.in/urfave/cli.v1 v1.20.0 // indirect
)
