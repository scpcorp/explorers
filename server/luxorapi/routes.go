package luxorapi

import "gitlab.com/scpcorp/explorers/server/types"

var Routes = types.Routes {
	{
		"GetSCPStautus",
		"GET",
		"/luxor/miner/status",
		GetSCPStatusHandler,
	},
}
