package luxorapi

import (
	"encoding/json"
	"fmt"
	"gitlab.com/NebulousLabs/errors"
	"io"
	"io/ioutil"
	"net/http"
)

var (
	luxorGet map[string] interface{}
)

// GetSCPStatusHandler get SCP stats from luxor
func GetSCPStatusHandler(w http.ResponseWriter, r *http.Request) {
	apiErr := get("http://mining.luxor.tech/api/SCP/stats/", &luxorGet)

	if apiErr != nil {
		fmt.Println("GetAllHost ERR: \n", apiErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// return json
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(luxorGet)

	if _, isJSONErr := err.(*json.SyntaxError); isJSONErr {
		panic("failed to return json")
	}
}

// HttpGET is a utility function for making http get requests to sia with a
// whitelisted user-agent. A non-2xx response does not return an error.
func httpGET(url string) (resp *http.Response, err error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	return http.DefaultClient.Do(req)
}

func drainAndClose(rc io.ReadCloser) {
	io.Copy(ioutil.Discard, rc)
	rc.Close()
}

// get requests the specified resource. The response, if provided, will be
// decoded into obj. The resource is the full API url.
func get(resource string, obj interface{}) error {
	// Request resource
	data, err := getRawResponse(resource)
	if err != nil {
		return err
	}
	if obj == nil {
		// No need to decode response
		return nil
	}

	if err := json.Unmarshal(data, obj); err != nil {
		panic(err)
	}

	//TODO: figure out what this does
	/*if err != nil {
		return errors.AddContext(err, "could not read response")
	}*/

	return nil
}

// get the raw http response and read body
func getRawResponse(resource string) ([]byte, error) {
	resp, err := httpGET(resource)
	fmt.Println("HTTP_GET:", resp, err)
	if err != nil {
		return nil, errors.AddContext(err, "request failed")
	}
	defer drainAndClose(resp.Body)

	if resp.StatusCode == http.StatusNotFound {
		return nil, errors.New("API call not recognized: " + resource)
	}

	if resp.StatusCode == http.StatusNoContent {
		// no reason to read the response
		return []byte{}, nil
	}
	return ioutil.ReadAll(resp.Body)
}