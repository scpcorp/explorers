import * as bigInt from 'big-integer';

export declare interface NormalizedHostData {
  acceptingContracts: any;
  baseRpcPrice: any;
  collateral: any;
  contractPrice: any;
  downloadBandwidthPrice: any;
  ephemeralAccountExpiry: any;
  keyValueDeletePrice: any;
  keyValueGetPrice: any;
  keyValueSetPrice: any;
  latency: any;
  maxCollateral: any;
  maxDownloadBatchSize: any;
  maxDuration: any;
  maxEphemeralAccountBalance: any;
  maxReviseBatchSize: any;
  netAddress: any;
  publicKey: any;
  remainingStorage: any;
  revisionNumber: any;
  sectorAccessPrice: any;
  sectorSize: any;
  sessionCreationTime: any;
  settingsResponseTime: any;
  siaMuxPort: any;
  storagePrice: any;
  timeStamp: any;
  totalScanTime: any;
  totalStorage: any;
  unlockHash: any;
  uploadBandwidthPrice: any;
  usedStorage: any;
  verified: any;
  version: any;
  windowSize: any;
}


export class HostStorageSum {
  used = 0;
  remaining = 0;
  total = 0;
  private hosts: Array<any>;

  constructor(hosts: Array<any>) {
    this.hosts = hosts;
  }

  calculateSum() {
    this.hosts.forEach(h => {
      this.sum(parseInt(h.usedStorage) || 0,
        parseInt(h.remainingStorage) || 0,
        parseInt(h.totalStorage) || 0);
    });
    return this;
  }

  getTB() {
    return {
      used: this.used / 1000,
      remaining: this.remaining / 1000,
      total: this.total / 1000
    };
  }

  // values are sum in GB according to the datatable
  private sum(u, r, t) {
    this.used += u;
    this.remaining += r;
    this.total += t;
  }

}

export class HostAveragePrice {
  contract = 0;
  storage = 0;
  bwUpload = 0;
  bwDownload = 0;
  collateral = 0;
  private hosts: Array<any>;

  constructor(hosts: Array<any>) {
    this.hosts = hosts;
  }

  calculateAverage() {
    this.hosts.forEach(h => {
      this.contract += parseFloat(h.contractPrice);
      this.storage += parseFloat(h.storagePrice);
      this.bwUpload += parseFloat(h.uploadBandWidthPrice);
      this.bwDownload += parseFloat(h.downloadBandWidthPrice);
      this.collateral += parseFloat(h.collateralPrice);
    });

    /*console.log('Total Storage', this.storage);
    console.log('Total Collateral', this.collateral);*/
    const l = this.hosts.length;
    this.contract = this.contract / l;
    this.storage = this.storage / l;
    this.bwUpload = this.bwUpload / l;
    this.bwDownload = this.bwDownload / l;
    this.collateral = this.collateral / l;

    return this;
  }

  fixDecimal(frac) {
    return {
      contract: this.contract.toFixed(frac),
      storage: this.storage,
      bwUpload: this.bwUpload.toFixed(frac),
      bwDownload: this.bwDownload.toFixed(frac),
      collateral: this.collateral
    };
  }

}

export const BytesPerGigabyte: any = bigInt(1e9);
export const BlockBytesPerMonthGigabyte: any = BytesPerGigabyte.times(4320);

export const getBytes = bytes => {

  if (!bytes) {
    return null;
  }
  return (bytes / BytesPerGigabyte.abs().toJSNumber()).toString().match(/[0-9]*/)[0];
};

export const hastingToSCP = hasting => {
  if (!hasting) {
    return null;
  }
  return (parseInt(hasting) / Math.pow(10, 24)).toFixed(2);
};

export const getBandWidthPrice = hasting => {
  if (!hasting) {
    return null;
  }

  return BytesPerGigabyte.times(bigInt(hasting))
    .abs()
    .toJSNumber() / Math.pow(10, 24);
};
