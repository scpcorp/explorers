import { createAction, props } from '@ngrx/store';

export enum HostActionTypes {
  LoadAllHost = '[Host] Load All',
  LoadHostsSuccess = '[Host API] Hosts Loaded Success',
  ActiveHosts = '[Host] Active',
  offlineHosts = '[Host] Online',
  LoadHostGigabytePerMonth = '[Host] Load Host Gigabyte Per Month',
  LoadHostGigabytePerMonthSuccess = '[Host API] Host GigabytePerMonth Load Success'
}

export const loadAllHosts = createAction(HostActionTypes.LoadAllHost);
export const activeHosts = createAction(HostActionTypes.ActiveHosts);
export const offlineHosts = createAction(HostActionTypes.offlineHosts);
export const loadHostsSuccess = createAction(
  HostActionTypes.LoadHostsSuccess,
  props<{ payload: any }>()
);

export const loadProviders = createAction(HostActionTypes.LoadHostGigabytePerMonth);
export const loadProvidersSuccess = createAction(
  HostActionTypes.LoadHostGigabytePerMonthSuccess,
  props<{ scans: Array<any> }>()
);
