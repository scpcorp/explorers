import { Injectable } from '@angular/core';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { HostService } from '../../services/host/host.service';
import {
  loadAllHosts,
  loadProviders,
  loadProvidersSuccess,
  loadHostsSuccess
} from './host.actions';

@Injectable()
export class HostEffects {
  constructor(private action$: Actions, private hs: HostService) {
  }

  loadHosts$ = createEffect(() => this.action$
    .pipe(
      ofType(loadAllHosts),
      mergeMap(() => this.hs.getAllHosts()
        .pipe(
          map(hosts => loadHostsSuccess({payload: hosts})),
          catchError(() => EMPTY)
        ))
    ));


  loadProviders$ = createEffect(() => this.action$
    .pipe(
      ofType(loadProviders),
      mergeMap(() => this.hs.getLatestHostScans()
        .pipe(
          map(hosts => loadProvidersSuccess({scans: hosts.scans})),
          catchError(() => EMPTY)
        ))
    ));

}

