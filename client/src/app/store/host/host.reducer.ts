import * as Host from './host.actions';
import * as bigInt from 'big-integer';

import { Action, createReducer, on } from '@ngrx/store';
import {
  BlockBytesPerMonthGigabyte,
  getBandWidthPrice,
  getBytes,
  hastingToSCP, HostAveragePrice, HostStorageSum,
  NormalizedHostData
} from './host-overview';

export interface State {
  all: Array<any>;
  active: Array<any>;
  offline: Array<any>;
  sum: any;
  average: any;
}

const initialState: State = {
  all: [],
  active: [],
  offline: [],
  sum: null,
  average: null
};


const mapHostStatus = (scans: Array<any>) => {
  const active: Array<any> = [];
  const offline: Array<any> = [];
  const all: Array<any> = scans;

  scans.forEach(h => {
    if (h.verified) {
      active.push(h);
    } else {
      offline.push(h);
    }
  });

  return {all, active, offline};
};

const normalizeHostData = (host): NormalizedHostData => {

  // calculate the Monthly Storage price
  // currently handled by host scans
  const storagePrice = (BlockBytesPerMonthGigabyte.times(bigInt(host.storageprice)))
    .abs()
    .toJSNumber() / Math.pow(10, 24);
  const collateralPrice = (BlockBytesPerMonthGigabyte.times(bigInt(host.collateral)))
    .abs()
    .toJSNumber() / Math.pow(10, 24);


  return {
    acceptingContracts: host.acceptingcontracts,
    baseRpcPrice: hastingToSCP(host.baserpcprice),
    collateral: hastingToSCP(host.collateral),
    contractPrice: hastingToSCP(host.contractprice),
    downloadBandwidthPrice: getBandWidthPrice(host.downloadbandwidthprice)?.toFixed(2),
    ephemeralAccountExpiry: hastingToSCP(host.ephemeralaccountexpiry),
    keyValueDeletePrice: hastingToSCP(host.keyvaluedeleteprice),
    keyValueGetPrice: hastingToSCP(host.keyvaluegetprice),
    keyValueSetPrice: hastingToSCP(host.keyvaluesetprice),
    latency: host.latency,
    maxCollateral: hastingToSCP(host.maxcollateral),
    maxDownloadBatchSize: host.maxdownloadbatchsize,
    maxDuration: host.maxduration,
    maxEphemeralAccountBalance: hastingToSCP(host.maxephemeralaccountbalance),
    maxReviseBatchSize: host.maxrevisebatchsize,
    netAddress: host.netaddress,
    publicKey: host.publickey,
    remainingStorage:  getBytes(host.remainingstorage),
    revisionNumber: host.revisionnumber,
    sectorAccessPrice: hastingToSCP(host.sectoraccessprice),
    sectorSize: host.sectorsize,
    sessionCreationTime: host.sessioncreationtime,
    settingsResponseTime: host.settingsresponsetime,
    siaMuxPort: host.siamuxport,
    storagePrice: storagePrice.toFixed(2),
    timeStamp: host.timestamp,
    totalScanTime: host.totalscantime,
    totalStorage:  getBytes(host.totalstorage),
    unlockHash: host.unlockhash,
    // uploadBandwidthPrice: hastingToSCP(host.uploadbandwidthprice),
    uploadBandwidthPrice: getBandWidthPrice(host.uploadbandwidthprice)?.toFixed(2),
    usedStorage: getBytes(host.totalstorage - host.remainingstorage),
    verified: host.verified,
    version: host.version,
    windowSize: host.windowsize,
  };
};


const hostReducer = createReducer(
  initialState,
  on(Host.loadHostsSuccess, (state, {payload}) => ({...state, ...mapHostStatus(payload)})),
  on(Host.offlineHosts, state => ({...state, offline: state.offline})),
  on(Host.activeHosts, state => ({...state, active: state.active})),
  on(Host.loadProvidersSuccess, (state, {scans}) => {
    const hostBuckets = mapHostStatus(scans);

    // perform price calculations
    const active = hostBuckets.active.map(h => normalizeHostData(h));
    const all = scans.map(h => normalizeHostData(h));
    const offline = hostBuckets.offline.map(h => normalizeHostData(h));

    // get average prices
    const average = new HostAveragePrice(active).calculateAverage().fixDecimal(2);
    const sum = new HostStorageSum(active).calculateSum().getTB();
    // console.log(average, sum)
    return {
      ...state,
      ...{all, active, offline, sum, average}
    };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return hostReducer(state, action);
}

