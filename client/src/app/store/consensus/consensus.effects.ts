import { Injectable } from '@angular/core';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { BlockQueryParam, ConsensusService } from '../../services/consensus/consensus.service';
import {
  getConsensus,
  getConsensusBlock,
  loadBlockResponse, loadConsensusResponse
} from './consensus.action';

@Injectable()
export class ConsensusEffects {
  constructor(private action$: Actions, private cs: ConsensusService) {
  }

  loadConsensus$ = createEffect(() => this.action$
    .pipe(
      ofType(getConsensus),
      mergeMap(() => this.cs.getConsensus()
        .pipe(
          map(consensus => (loadConsensusResponse({payload: consensus}))),
          catchError(() => EMPTY)
        )),
      tap(c => console.log('LOAD CONS TAP', c))
    ));


  loadBlock$ = createEffect(() => this.action$
    .pipe(
      ofType(getConsensusBlock),
      switchMap(() => this.cs.getConsensus()),
      mergeMap((response) => this.cs.blocks({query: BlockQueryParam.height, value: response.height})
        .pipe(
          map(block => (loadBlockResponse({payload: block}))),
          catchError(() => EMPTY)
        ))
    ));
}
