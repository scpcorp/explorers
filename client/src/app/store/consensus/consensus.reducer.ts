import * as Consensus from './consensus.action';
import { Action, createReducer, on } from '@ngrx/store';

export interface State {
  status: any;
  block: any;
}

const initialState: State = {
  status: null,
  block: null,
};


const consensusReducer = createReducer(
  initialState,
  on(Consensus.loadConsensusResponse, (state, { payload }) => ({...state, status: payload})),
  on(Consensus.loadBlockResponse, (state, { payload }) => ({...state, block: payload}))
);


export function reducer(state: State | undefined, action: Action) {
  return consensusReducer(state, action);
}
