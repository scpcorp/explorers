import {createAction, props} from '@ngrx/store';


enum ConsensusActionTypes {
  GetConsensus = '[Consensus] Get Consensus',
  LoadConsensusResponse = '[Consensus API] Consensus Response Success',
  LoadBlockResponse = '[Consensus API] Block Loaded Success',
  GetConsensusBlock = '[Consensus] Get Block',
}

export const getConsensus = createAction(ConsensusActionTypes.GetConsensus);
export const getConsensusBlock = createAction(ConsensusActionTypes.GetConsensusBlock);


export const loadBlockResponse = createAction(
  ConsensusActionTypes.LoadBlockResponse,
  props<{ payload: any }>()
);


export const loadConsensusResponse = createAction(
  ConsensusActionTypes.LoadConsensusResponse,
  props<{ payload: any }>()
);
