import { createAction, props } from '@ngrx/store';

export enum ActionTypes {
  SendSearch = '[Consensus Hash API] Send Search',
  LoadSearchResult = '[Consensus Hash API] Load Search Response Success',
}

export const navSearch = createAction(
  ActionTypes.SendSearch,
  props<{ query: string }>()
);

export const navSearchSuccess = createAction(
  ActionTypes.LoadSearchResult,
  props<{ [k: string]: any }>()
);
