import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, concatMap, map, tap } from 'rxjs/operators';
import { navSearch, navSearchSuccess } from './search.actions';
import { EMPTY } from 'rxjs';
import { Router } from '@angular/router';
import { SearchService } from '../../services/search/search.service';

@Injectable()
export class SearchEffects {
  constructor(private actions$: Actions,
              private router: Router,
              private ss: SearchService) {
  }

  navSearch$ = createEffect(() => this.actions$.pipe(
    ofType(navSearch),
    concatMap((props) => this.ss.searchDocID(props.query)
      .pipe(
        // I want to dispatch to router here
        map(res => navSearchSuccess(res)),
        catchError(() => EMPTY)
      ))
    )
  );

  // search result are loaded into SearchResultResolverService `extras` field.
  // on-click navigation to trans, blocks etc.. should be handled via
  // stores or have resolver load to store if needed.
  navSearchSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(navSearchSuccess),
    map(res => res?.hits?.hits[0]),
    tap(hit => this.router.navigate(routeIndexType(hit), {state: isSelected(hit)}))
  ), {dispatch: false});

}


const routeIndexType = (hit: any): any[] => {
  switch (hit._index) {
    case 'transactionid':
      return ['/transaction'];
    case 'scprime_consensus':
      return ['/block/', hit._id];
    case 'test1_hostdb_scan':
      return ['/provider/', hit._id];
    default:
      return ['/']; // TODO: handle error and empty search result
  }
};


// we have two type of data for routing to a component
// it is either a listView or SelectedView
// and we want the routing component to know which view to display
// by using a selected boolean to identify if the return data is
// an array or object. That way, the view knows to display list or detailed view
const isSelected = (data: any) => {
  if (Array.isArray(data)) {
    return {...{list: data}, ...{selected: false}};
  }
  return {...data, ...{selected: true}};

};
