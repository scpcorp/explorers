import { NgModule } from '@angular/core';
import { reducers, metaReducers } from './reducers';
import { AppEffects } from './app.effects';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { ConsensusEffects } from './store';
import { ProviderModule } from './modules/provider/provider.module';
import { SharedModules } from './share/shared.module';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule, routerReducer, NavigationActionTiming } from '@ngrx/router-store';
import { RouterModule } from '@angular/router';
import { SearchEffects } from './store/search/search.effects';
import { CustomSerializer } from './custom-route-serializer';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProviderModule,
    HttpClientModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({... reducers, ...{router: routerReducer}}, {metaReducers}),
    RouterModule.forRoot([], { onSameUrlNavigation: 'reload', relativeLinkResolution: 'legacy' }),
    // turn on for StoreRouterConfig to use custom route serializer
    // use {navigationActionTiming: NavigationActionTiming.PostActivation}
    // to dispatched after guards and resolvers
    StoreRouterConnectingModule.forRoot({
      // serializer: CustomSerializer, // NOTE: turn on if we use custom router-store-selector
      navigationActionTiming: NavigationActionTiming.PostActivation
    }),
    EffectsModule.forRoot(
      [
        AppEffects,
        ConsensusEffects,
        SearchEffects,
      ]),
    SharedModules,
    StoreRouterConnectingModule.forRoot()
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}

