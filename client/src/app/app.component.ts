import { Component } from '@angular/core';

@Component({
  selector: 'spc-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'network-provider';

}
