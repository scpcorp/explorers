import { Component, OnInit } from '@angular/core';
import { onMainContentChange, onSideNavChange } from 'src/app/animations/animations';
import { SideNavService } from 'src/app/services/sidenav.service';

@Component({
  selector: 'spc-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss'],
  animations: [onSideNavChange, onMainContentChange]
})
export class AppShellComponent {
  public onSideNavChange: boolean;

  constructor(private _sidenavService: SideNavService) {
    this._sidenavService.sideNavState$.subscribe( res => {
      console.log(res)
      this.onSideNavChange = res;
    })
  }

}
