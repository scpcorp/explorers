import { Component, OnInit } from '@angular/core';
import { ConsensusService } from '../../../../services/consensus/consensus.service';
import { Store, select } from '@ngrx/store';
import { selectBlock, selectConsensusStatus, selectRouteId, selectCurUrl, selectCurUrlData, selectHosts } from '../../../../reducers';
import { combineLatest, Observable } from 'rxjs';
import { combineAll, shareReplay, withLatestFrom } from 'rxjs/operators';
import { getConsensus, getConsensusBlock } from '../../../../store/consensus/consensus.action';
import { MinerService } from '../../../../services/miner/miner.service';
import { MarketPriceService } from '../../../../services/market-price/market-price.service';

interface Consensus {
  synced: boolean;
  height: number;
  currentBlock: string;
  target: Array<number>;
}

interface SouthXAPIResponse {
  tickers: Array<any>;
}

interface LuxorAPIResponse {
  fee: number;
  price: number;
  hashrate: number;
  height: number;
  difficulty: number;
  prevHash: string;
  blockSubsidy: number;
  blocksFound: number;
  totalMinders: number;
  globalStats: Array<any>;
}


@Component({
  selector: 'spc-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  consensus$: Observable<any> = this.store.pipe(select(selectConsensusStatus), shareReplay(1));
  block$: Observable<any> = this.store.pipe(select(selectBlock), shareReplay(1));
  hosts$: Observable<any> = this.store.pipe(select(selectHosts), shareReplay(1));

  btcPrice: any;
  ltcPrice: any;
  minerStatus: any;

  constructor(private cs: ConsensusService, private store: Store<any>,
              private ms: MinerService, private mp: MarketPriceService) {
  }

  ngOnInit() {
    this.loadCurBlock();

    this.mp.getSouthXChangePrice()
      .subscribe((res: SouthXAPIResponse) => this.filterPrice(res.tickers));

    this.ms.luxorApi()
      .subscribe((res: LuxorAPIResponse) => {
        console.log('LUXOR_API', res);
        this.minerStatus = res;
      });
  }

  private loadCurBlock() {
    this.store.dispatch(getConsensus());
    this.store.dispatch(getConsensusBlock());
  }

  private filterPrice(tickers) {
    tickers.forEach(tick => {
      if (tick.target === 'LTC') {
        this.ltcPrice = tick;
      }
      if (tick.target === 'BTC') {
        this.btcPrice = tick;
      }
    });
  }

}
