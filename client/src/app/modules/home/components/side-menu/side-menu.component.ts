import { Component, OnInit } from '@angular/core';
import { SideNavService } from '../../../../services/sidenav.service';
import { animateText, onSideNavChange } from '../../../../animations/animations';

interface Page {
  link: string;
  name: string;
  icon: string;
}

@Component({
  selector: 'spc-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
  animations: [animateText, onSideNavChange]
})
export class SideMenuComponent implements OnInit {
  public sideNavState = false;
  public linkText = false;

  public pages: Page[] = [
    {name: 'Provider List', link:'provider', icon: 'monetization_on'},
    {name: 'Storage Market', link:'storage-market', icon: 'shopping_bag'},
    {name: 'Coin Metrics', link:'some-link', icon: 'attach_money'},
  ];

  constructor(private _sidenavService: SideNavService) { }

  ngOnInit() {
  }

  onSinenavToggle() {
    this.sideNavState = !this.sideNavState;

    setTimeout(() => {
      this.linkText = this.sideNavState;
    }, 200);
    this._sidenavService.sideNavState$.next(this.sideNavState);
  }

}
