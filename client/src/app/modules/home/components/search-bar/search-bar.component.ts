import { Component, OnInit } from '@angular/core';
import { ExplorerService } from '../../../../services/consensus/explorer.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { navSearch } from '../../../../store/search/search.actions';

@Component({
  selector: 'spc-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  form: FormGroup;
  constructor(private fb: FormBuilder, private es: ExplorerService, private store: Store<any>) { }

  ngOnInit() {
    this.form = this.fb.group({
      searchStr: [null, Validators.required]
    });



  }

  onSubmit(form) {
    if (!form.valid) {
      return;
    }
    console.log('Searching', form.value.searchStr);
    this.store.dispatch(navSearch({query: form.value.searchStr}));
  }

}
