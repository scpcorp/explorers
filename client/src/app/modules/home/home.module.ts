import { NgModule } from '@angular/core';
import { SharedModules } from '../../share/shared.module';
import { HomeComponent } from './home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { SearchResultResolverService } from '../../services/search-result-resolver.service';
import { AppShellComponent } from './components/app-shell/app-shell.component';
import { HeaderComponent } from './components/header/header.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';

const routes: Routes = [
  {
    path: '',
    component: AppShellComponent,
    // resolve: {searchResult: SearchResultResolverService},
    // runGuardsAndResolvers: 'always',
    children: [
      {
        path: 'provider',
        loadChildren: () => import('../provider/provider.module').then(m => m.ProviderModule)
      },
      {
        path: 'block',
        loadChildren: () => import('../block/block.module').then(b => b.BlockModule)
      },
      {
        path: 'transaction',
        loadChildren: () => import('../transaction/transaction.module').then(m => m.TransactionModule)
      },
      {
        path: 'storage-market',
        loadChildren: () => import('../storage-market/storage-market.module').then(sm => sm.StorageMarketModule)
      },
    ]
  },

];

@NgModule({
  declarations: [
    HomeComponent,
    DashboardComponent,
    SearchBarComponent,
    AppShellComponent,
    HeaderComponent,
    SideMenuComponent,
  ],
  imports: [RouterModule.forChild(routes), SharedModules],
  exports: [RouterModule, HomeComponent]
})
export class HomeModule {
}
