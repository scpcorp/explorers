import { NgModule } from '@angular/core';
import { ProviderComponent } from './provider.component';
import { SharedModules } from '../../share/shared.module';
import { ProviderDetailComponent } from './components/provider-detail/provider-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { HostEffects } from '../../store';
import { ProviderListComponent } from './components/provider-list/provider-list.component';
import { SearchResultResolverService } from '../../services/search-result-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: ProviderComponent
  },
  {
    path: ':pubkey',
    component: ProviderDetailComponent,
    resolve: {searchResult: SearchResultResolverService},
  }
];

@NgModule({
  declarations: [
    ProviderComponent,
    ProviderDetailComponent,
    ProviderListComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModules,
    EffectsModule.forFeature([HostEffects])
  ],
  exports: [ProviderComponent]
})
export class ProviderModule {
}
