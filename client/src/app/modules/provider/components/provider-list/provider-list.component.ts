import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'spc-provider-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProviderListComponent implements OnChanges {
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @Input() hosts: Array<any>;
  @Output() hostSelect: EventEmitter<any> = new EventEmitter<any>();

  _hosts: any;

  displayedColumns = [
    'netAddress',
    'usedStorage',
    'remainStorage',
    'totalStorage',
    'contractPrice',
    'storagePrice',
    'uploadBandWidthPrice',
    'downloadBandWidthPrice',
    'collateralPrice',
    // 'firstSeen',
    // 'uptime',
    // 'scanHistory',
  ];

  footerColumns = [
    'address',
    'usedStorage',
    'remainStorage',
    'totalStorage',
    'contractPrice',
  ];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.hosts) {
      this._hosts = new MatTableDataSource(this.hosts);
      this._hosts.sort = this.sort;

      // console.log('SCAN',this.hosts[0].scanHistory):q
    }

  }

  // TODO: remove later once we move price calculation to server
  mostRecentScan(scans: Array<any>) {
    const mostRecent = scans.find(s => s.success === true);
    return mostRecent ? mostRecent.timestamp : null;
  }
}
