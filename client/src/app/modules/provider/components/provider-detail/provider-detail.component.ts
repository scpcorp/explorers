import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'spc-provider-detail',
  templateUrl: 'provider-detail.component.html',
  styleUrls: ['provider-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProviderDetailComponent implements OnChanges {
  @Input() host: any;
  @Output() closeView = new EventEmitter<any>();
  searched$ = this.route.data;

  constructor(private route: ActivatedRoute) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes.host);
    console.log(this.host);
  }


}
