import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { selectHosts } from '../../reducers';
import { parse } from 'json2csv';
import { getConsensus } from '../../store/consensus/consensus.action';
import { loadProviders } from '../../store/host/host.actions';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';




@Component({
  selector: 'spc-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {
  hosts$: Observable<any> = this.store.pipe(select(selectHosts), shareReplay(1));
  hostDetail: any = null;
  hostLabel: string;

  constructor(private store: Store<any>) {
  }

  ngOnInit() {
    this.store.dispatch(loadProviders());
    setTimeout(() => {
      this.store.dispatch(loadProviders());
    }, 300000);

  }

  // TODO: export to CSV, move to server later
  exportToCSV(hosts: Array<any>) {
    const fields = [
      'acceptingContracts', 'netAddress', 'usedStorage', 'remainingStorage',
      'totalStorage', 'contractPrice', 'storagePrice', 'collateralPrice', 'uploadBandWidthPrice',
      'downloadBandWidthPrice'
    ];

    const tsfOpts = {encoding: 'utf-8'};
    const opts = {fields, withBOM: true};

    try {
      const csv = parse(hosts, opts, tsfOpts);
      const csvContent = `data:text/csv;charset=utf-8,${csv}`;
      const encodedUri = encodeURI(csvContent);
      window.open(encodedUri);
    } catch (err) {
      console.log('CSV Export Err:', err);
    }
  }

}
