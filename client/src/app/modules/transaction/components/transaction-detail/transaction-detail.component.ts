import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'spc-transaction-detail',
  templateUrl: './transaction-detail.component.html',
  styleUrls: ['./transaction-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransactionDetailComponent implements OnInit {
  @Input() transaction;

  tx = {
    "filecontractids": null,
    "filecontractmissedproofoutputids": null,
    "filecontractrevisionmissedproofoutputids": null,
    "filecontractrevisionvalidproofoutputids": null,
    "filecontractvalidproofoutputids": null,
    "height": 46654,
    "id": "a85a7f6a8a5702513184a3659d82b096d691aaa87d8b3196b01dda2c83db2b64",
    "parent": "00000000000000098a6b8c87bad4c48ac7307f8e8c3ff488227fdcee55ca4d36",
    "rawtransaction": {
      "arbitrarydata": [],
      "filecontractrevisions": [],
      "filecontracts": [],
      // Sum of Miner FEES
      "minerfees": [
        "63600000000000000000000"
      ],
      "siacoininputs": [
        {
          "parentid": "189bbeb3bde6c9ca60d371fcc0a4087f43d90bfac533849aa0482246c682e108",
          "unlockconditions": {
            "publickeys": [
              {
                "algorithm": "ed25519",
                "key": "jrupLDyYvLu/IL6ESOqpqSpiWnraMUl1H8FHIkesiUw="
              }
            ],
            "signaturesrequired": 1,
            "timelock": 0
          }
        }
      ],
      // RECEIVER(S)
      "siacoinoutputs": [
        {
          "unlockhash": "69cb79ca0aca4c69e3a9520a94ad9c96ca4bfde5b8868e4880d35b8b3df02e11240d21e3aeb4",
          "value": "270797078120850335562000000000"
        },
        {
          "unlockhash": "1aefbd22e793372912874e4000277a61e59188a48dd28e87755d97628b4d8a9940d92da832dc",
          "value": "8173774089953714334049566000000"
        }
      ],
      "siafundinputs": [],
      "siafundoutputs": [],
      "storageproofs": [],
      "transactionsignatures": [
        {
          "coveredfields": {
            "arbitrarydata": [],
            "filecontractrevisions": [],
            "filecontracts": [],
            "minerfees": [],
            "siacoininputs": [],
            "siacoinoutputs": [],
            "siafundinputs": [],
            "siafundoutputs": [],
            "storageproofs": [],
            "transactionsignatures": [],
            "wholetransaction": true
          },
          "parentid": "189bbeb3bde6c9ca60d371fcc0a4087f43d90bfac533849aa0482246c682e108",
          "publickeyindex": 0,
          "signature": "WTrpnPhSEsK3BGPq6UZNHm1CywPXnaPKP33nchFpH2hJxxl2FsjxB+K+zQkGKQDaQYewOsNqPHdkRUHQfULpDQ==",
          "timelock": 0
        }
      ]
    },
    // SENDER
    "siacoininputoutputs": [
      {
        "unlockhash": "467430b10f9abbdffd5632cafd76bdbb91e14ff4e846514da55b186e88d7188c8620b0930e36",
        "value": "8444571231674564669611566000000"
      }
    ],
    "siacoinoutputids": [
      "e73cca988fc2291f5a1c52e9478bfb85c7319d3871ceda2aeb865383e4a1654b",
      "e371728fd572f59061d26f30b2a4f024ab15ad77d9cc44757e535713e251f5b5"
    ],
    "siafundclaimoutputids": null,
    "siafundinputoutputs": null,
    "siafundoutputids": null,
    "storageproofoutputids": null,
    "storageproofoutputs": null
  }

  constructor() { }

  ngOnInit() {
  }

}
