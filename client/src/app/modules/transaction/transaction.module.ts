import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionComponent } from './transaction.component';
import { SharedModules } from '../../share/shared.module';
import { TransactionListComponent } from './components/transaction-list/transaction-list.component';
import { TransactionDetailComponent } from './components/transaction-detail/transaction-detail.component';
import { SearchResultResolverService } from '../../services/search-result-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: TransactionComponent,
    resolve: {searchResult: SearchResultResolverService},
    runGuardsAndResolvers: 'always'
  },
];

@NgModule({
  declarations: [TransactionComponent, TransactionListComponent, TransactionDetailComponent],
  imports: [RouterModule.forChild(routes), SharedModules],
  exports: [RouterModule, TransactionComponent]
})
export class TransactionModule { }
