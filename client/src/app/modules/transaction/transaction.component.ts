import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { selectCurUrlData } from '../../reducers';
import { select, Store } from '@ngrx/store';
import { map, shareReplay } from 'rxjs/operators';
import { Data } from '@angular/router';

@Component({
  selector: 'spc-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  // TODO: when we have a transaction store, we will merge the store and router-store data into
  // one Obs, that would clean up the view.
  txRouteData$: Observable<Data> = this.store.pipe(select(selectCurUrlData),
    map(data => data.searchResult), shareReplay(1));

  constructor(private store: Store<any>) {
  }

  ngOnInit() {
    this.txRouteData$
      .subscribe(data => console.log('TX?', data));

  }

}
