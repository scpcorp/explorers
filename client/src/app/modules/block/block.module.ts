import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlockDetailComponent } from './components/block-detail/block-detail.component';
import { SharedModules } from '../../share/shared.module';
import { SearchResultResolverService } from '../../services/search-result-resolver.service';

const routes: Routes = [
  {
    path: ':height',
    component: BlockDetailComponent,
    resolve: {searchResult: SearchResultResolverService},
  }
];



@NgModule({
  declarations: [BlockDetailComponent],
  imports: [RouterModule.forChild(routes), SharedModules],
  exports: [RouterModule]
})
// @ts-ignore
export class BlockModule {
}
