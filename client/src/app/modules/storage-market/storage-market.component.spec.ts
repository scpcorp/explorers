import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StorageMarketComponent } from './storage-market.component';

describe('StorageMarketComponent', () => {
  let component: StorageMarketComponent;
  let fixture: ComponentFixture<StorageMarketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StorageMarketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StorageMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
