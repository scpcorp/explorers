import { NgModule } from '@angular/core';
import { StorageMarketComponent } from './storage-market.component';
import { SharedModules } from '../../share/shared.module';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: StorageMarketComponent
  }
];


@NgModule({
  declarations: [StorageMarketComponent],
  imports: [RouterModule.forChild(routes), SharedModules],
  exports: [RouterModule]
})
// @ts-ignore
export class StorageMarketModule { }
