import {
  ActionReducerMap, createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import {
  HostReducer, HostState,
  ConsensusReducer, ConsensusState
} from '../store';

import { environment } from '../../environments/environment';
import * as fromRouter from '@ngrx/router-store';

export interface State {
  host: HostState;
  consensus: ConsensusState;
  router: fromRouter.RouterReducerState<any>;
}

export const reducers: ActionReducerMap<State> = {
  host: HostReducer,
  consensus: ConsensusReducer,
  router: fromRouter.routerReducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

// Router Selectors
export const selectRouter = createFeatureSelector<
  State,
  fromRouter.RouterReducerState<any>
  >('router');

const {
  selectQueryParams,    // select the current route query params
  selectQueryParam,     // factory function to select a query param
  selectRouteParams,    // select the current route params
  selectRouteParam,     // factory function to select a route param
  selectRouteData,      // select the current route data
  selectUrl,            // select the current url
} = fromRouter.getSelectors(selectRouter);

export const selectRouteId = selectRouteParam('id');
export const selectStatus = selectQueryParam('status');
export const selectCurUrl = selectUrl;
export const selectCurUrlData = selectRouteData;


// Host Selectors
export const selectHosts = (state: State) => state.host;
export const selectActiveHosts = createSelector(selectHosts, (host: any) => host.active);


// Consensus Selectors
export const selectConsensus = (state: State) => state.consensus;
export const selectConsensusStatus = createSelector(selectConsensus, con => con.status);
export const selectBlock = createSelector(selectConsensus, con => con.block);
