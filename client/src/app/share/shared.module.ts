import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CustomMaterialModules} from './material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PipeModules} from './pipe/pipes.module';
import {DirectiveModule} from './directive.module';
import {MomentModule} from 'ngx-moment';


@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModules,
    DirectiveModule,
    FormsModule,
    MomentModule.forRoot({
      relativeTimeThresholdOptions: {
        m: 59
      }
    }),
    ReactiveFormsModule,
    RouterModule,
    PipeModules,
  ],
  declarations: [],
  exports: [
    CommonModule,
    CustomMaterialModules,
    DirectiveModule,
    FormsModule,
    MomentModule,
    ReactiveFormsModule,
    RouterModule,
    PipeModules,
  ]
})
export class SharedModules {
}
