import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Renderer2
} from '@angular/core';
import { tz } from 'moment-timezone';
import * as moment from 'moment';


@Directive({
  selector: '[spcTimeElapse]',
})
export class TimeElapseDirective implements OnInit, OnDestroy, OnChanges {
  @Input('spcTimeElapse') timeStamp: number | string;

  timer: any = null;
  elapsedEl: any;
  date: any | moment.Moment;
  zone: any;

  constructor(
    private el: ElementRef,
    private r: Renderer2) {
  }

  ngOnInit(): void {
    this.elapsedEl = this.r.createElement('span');
    this.r.appendChild(this.el.nativeElement, this.elapsedEl);

  }

  ngOnChanges(): void {
    if (!this.timeStamp) {
      return;
    }

    this.zone = tz.guess();
    this.date = moment.unix(Number(this.timeStamp));
    this.setElapsedTime();
  }

  ngOnDestroy(): void {
    clearInterval(this.timer);
  }

  private setElapsedTime(): void {
    // clear timer if any to avoid mem leaks
    if (this.timer) {
      clearInterval(this.timer);
    }

    this.startDuration();
    this.timer = setInterval(this.startDuration.bind(this), 1000);
  }

  private startDuration(): void {
    if (!this.elapsedEl) {
      return;
    }
    const local = this.date.tz(this.zone).format('MM/D/YYYY, H:mm:ss');
    const duration = moment.duration(moment().diff(this.date));
    const hours = `${!!duration.hours() ? `${duration.hours()}h ` : ''}`;
    const minutes = `${!!duration.minutes() ? `${duration.minutes()}m ` : ''}`;
    const seconds = `${!!duration.seconds() ? `${duration.seconds()}s ` : ''}`;

    this.r.setProperty(this.elapsedEl, 'innerText', ` ${local} (${hours}${minutes}${seconds} ago)`);
  }

}
