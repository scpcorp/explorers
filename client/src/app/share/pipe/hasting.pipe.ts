import {Pipe, PipeTransform} from '@angular/core';

/**
 * Pipe takes in Hasting and a currency unit, calculates price
 */
@Pipe({name: 'hasting'})
export class HastingPipe implements PipeTransform {

  transform(hasting: string, unit = 'SCP'): string {
    if (unit === 'SCP') return (parseInt(hasting) / Math.pow(10, 24)).toFixed(2);
    else return hasting;
  }
}
