import {Pipe, PipeTransform} from '@angular/core';

/*
`input | runFunction`
// runFunction(input)
`input | runFunction:[arg1]`
// runFunction(arg1)
`input | runFunction:[[arg1]]`
// runFunction([arg1])
*/

@Pipe({name: 'runFunction'})
export class RunFunctionPipe implements PipeTransform {
  transform(input: any, func: Function, args?: any[], trigger?: any): any {
    return func(...(Array.isArray(args) ? args : [input]));
  }
}
