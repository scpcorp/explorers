import {Pipe, PipeTransform} from '@angular/core';
import * as byte from 'bytes';

@Pipe({name: 'byteTo'})
export class ByteToPipe implements PipeTransform {
  readonly u: byte.Unit = 'GB';
  
  transform(bytes: number = 0, unit = this.u): string {
    return byte.format(bytes, {unit: unit, decimalPlaces: 0})?.match(/[0-9]*/)[0];
  }
}
