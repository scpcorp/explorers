import { NgModule } from '@angular/core';
import {TimeElapseDirective} from './directives/time-elapse.directive';


@NgModule({
  imports: [
  ],
  exports: [
    TimeElapseDirective,
  ],
  declarations: [TimeElapseDirective],
})
export class DirectiveModule {}
