import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { handleError } from '../../share/error/service-error-handler';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private api = '/v1/search';

  constructor(private http: HttpClient) {
  }

  searchDocID(text: any): Observable<any> {
    return this.http
      .get(`${this.api}/${text}`)
      .pipe(catchError(handleError));
  }

  getLatestProviderScans(): Observable<any> {
    return this.http
      .get('/v1/current-scans')
      .pipe(catchError(handleError));
  }

}
