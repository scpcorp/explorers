import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SearchResultResolverService {

  constructor(private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      const navExtraState = this.router.getCurrentNavigation().extras.state;
      console.log('SEARCH_STATE', navExtraState);

    // TODO: hostscan array out of memory error for displaying on page. Taking last
    // scan only, refactor this to server
      if (navExtraState?._source?.hostscans) {
        navExtraState._source = {
          created: navExtraState._source.created,
          netaddress: navExtraState._source.netaddress,
          hostscans: navExtraState._source.hostscans[navExtraState._source.hostscans.length - 1]
        };
      }

      return navExtraState || null;
  }

}
