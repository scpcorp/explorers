import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MarketPriceService {
  private endPoint = 'https://api.coingecko.com/api/v3/coins/siaprime-coin/tickers';
  constructor(private http: HttpClient) { }

  getSouthXChangePrice() {
    const query = new HttpParams().set('exchange_ids', 'south_xchange');
    return this.http
      .get(this.endPoint, {params: query})
  }
}
