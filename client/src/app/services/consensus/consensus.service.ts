import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { handleError } from '../../share/error/service-error-handler';


export enum BlockQueryParam {
  height = 'height',
  id = 'id'
}

export interface BlockAPIParams {
  query: BlockQueryParam;
  value: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConsensusService {
  private api = '/v1/consensus';

  constructor(private http: HttpClient) {
  }

  getConsensus(): Observable<any> {
    return this.http
      .get(this.api)
      .pipe(catchError(handleError));
  }

  blocks(param: BlockAPIParams): Observable<any> {
    return this.http
      .get(`${this.api}/blocks`, {params: new HttpParams().set(param.query, param.value)})
      .pipe(catchError(handleError));
  }
}
