import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { handleError } from '../../share/error/service-error-handler';

@Injectable({
  providedIn: 'root'
})
export class MinerService {
  private api = '/v1/luxor/miner/status';

  constructor(private http: HttpClient) {
  }

  luxorApi() {
    return this.http.get(this.api)
      .pipe(catchError(handleError));
  }
}
